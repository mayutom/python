# 2021/06/16 M. Tominaga

################################
# Imports
################################
import serial
import time
import argparse
import numpy as np
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
from sys import exit


BUSY = str(b'B\r\n')

STAGE_RANGE = 200. #mm
V_MIN = -1000 #pulse
V_MAX = 99559 #pulse
H_MIN = -1000 #pulse
H_MAX = 99595 #pulse

V_STEP = STAGE_RANGE/(V_MAX - V_MIN) #mm/pulse
H_STEP = STAGE_RANGE/(H_MAX - H_MIN) #mm/pulse
##################################
# Definitions
##################################
def check_pos(ser): # check current position
    ser.write(b"Q:\r\n")
    pos = str(ser.readline())
    print(pos)
    pos_h = int(pos.split(",")[0].split("'")[1].replace(" ",""))
    pos_v = int(pos.split(",")[1].replace(" ",""))
    print("Current position is", pos_h, pos_v)
    return pos_h, pos_v

def move_to_center(ser, pos_h, pos_v):
    # do check_pos(f=None) beforehand
    move_h = -1*int(pos_h)
    move_v = -1*int(pos_v)
    if move_h >= 0 and move_v >= 0:
        code="M:W+P"+str(int(move_h))+"+P"+str(int(move_v))+"\r\n"
    elif move_h >= 0 and move_v < 0:
        code="M:W+P"+str(int(move_h))+"-P"+str(int(-move_v))+"\r\n"
    elif move_h < 0 and move_v >= 0:
        code="M:W-P"+str(int(-move_h))+"+P"+str(int(move_v))+"\r\n"
    elif move_h < 0 and move_v < 0:
        code="M:W-P"+str(int(-move_h))+"-P"+str(int(-move_v))+"\r\n"

    return code

def move_exit(ser):
    move_h = int(70./H_STEP)
    move_v = int(70./V_STEP)
    
    code = "M:W+P"+str(move_h)+"+P"+str(move_v)+"\r\n"
    ser.write(b"D:1S3500F5000R50\r\n")
    ser.readline()
    ser.write(b"D:2S3500F5000R50\r\n")
    ser.readline()
    ser.write(code.encode())
    ser.readline()
    ser.write(b"G:\r\n")
    check_move(ser)
    ser.write(b"Q:\r\n")
    print("Exit position is", ser.readline())
    

def check_cmd(ser):
    a=str(ser.readline())
    print(a)
    if a != str(b'OK\r\n'):
        move_exit(ser)
        exit(1)

def check_move(ser):
    ser.write(b"!:\r\n")
    a=str(ser.readline())
    while True:
        if a == BUSY:
            time.sleep(0.5)
            ser.write(b"!:\r\n")
            a=str(ser.readline())
            print(a)
        else:
            break
    ser.read_all()

##################################
# Main routine
##################################
if __name__ == '__main__':

    #
    # command-line parser

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f','--file',
        help='cmd output file name',
        dest='file',
        default = "cmd.txt",
        nargs='?'
        )
    args = parser.parse_args()

    file_name = args.file

    print("Serial connection.")
    ser = serial.Serial(port="/dev/ttyUSB0",baudrate=9600, write_timeout=10, timeout=10)


    f = open(file_name, 'r')
    datalist = f.readlines()
    line = 0
    for data0 in datalist:
        line += 1
        print("Command line ", line)

        data0=data0.replace("\n","")
        data=str(data0)+"\r\n"
        print("Command is ", data.encode())
        
        if data[0] == "Q":
            ser.write(data.encode())
            print(ser.readline())
            
        elif data[0] == "D":

            # target = "F"
            # if data[2] == "1":
            #     idx = data.find(target)
            #     vel_h = int(data[4:idx])
            #     print("Vel_h =", vel_h)
            # else:
            #     idx = data.find(target)
            #     vel_v = int(data[4:idx])
            #     print("Vel_v =", vel_v)
            ser.write(data.encode())
            check_cmd(ser)
            
        elif data[0]=="A":
            if line <= 2:# move to zero point
                ser.write(b"D:1S3500F5000R50\r\n")
                check_cmd(ser)
                ser.write(b"D:2S3500F5000R50\r\n")
                check_cmd(ser)
                ser.write(data.encode())
                check_cmd(ser)
                ser.write(b"G:\r\n")
                check_cmd(ser)
                check_move(ser)

            else:
                ser.write(data.encode())                                                                                                                
                check_cmd(ser)                                                                                                                          
                ser.write(b"G:\r\n")
                check_cmd(ser)
                check_move(ser)

            '''
            ser.write(b"D:1S3500F5000R50\r\n")
            check_cmd(ser)
            ser.write(b"D:2S3500F5000R50\r\n")
            check_cmd(ser)
            ser.write(data.encode())
            check_cmd(ser)
            ser.write(b"G:\r\n")
            check_move(ser)
            '''

        elif data[0]=="M":
            if line <= 2:# move to zero point
                ser.write(b"D:1S3500F5000R50\r\n")
                check_cmd(ser)
                ser.write(b"D:2S3500F5000R50\r\n")
                check_cmd(ser)
                ser.write(data.encode())
                check_cmd(ser) 
                ser.write(b"G:\r\n")
                check_cmd(ser)
                check_move(ser)

            else:
                ser.write(data.encode())
                check_cmd(ser)
                ser.write(b"G:\r\n")
                check_cmd(ser)
                check_move(ser)
    f.close()

    # go back to the center relatively.
    pos_h, pos_v = check_pos(ser)
    code = move_to_center(ser, pos_h, pos_v)
    print(code)
    ser.write(b"D:1S3500F5000R50\r\n")
    check_cmd(ser)
    ser.write(b"D:2S3500F5000R50\r\n")
    check_cmd(ser)
    ser.write(code.encode())
    check_cmd(ser)
    ser.write(b"G:\r\n")
    check_cmd(ser)
    ser.write(b"Q:\r\n")
    time.sleep(1)

    print("Curretn position is ", ser.readline())

    ser.close()

