# 2021/06/16 M. Tominaga

################################
# Imports
################################
import serial
import time
import argparse
import numpy as np
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
from sys import exit


BUSY = str(b'B\r\n')

STAGE_RANGE = 200. #mm
V_MIN = -1000 #pulse
V_MAX = 99559 #pulse
H_MIN = -1000 #pulse
H_MAX = 99595 #pulse

V_STEP = STAGE_RANGE/(V_MAX - V_MIN) #mm/pulse
H_STEP = STAGE_RANGE/(H_MAX - H_MIN) #mm/pulse
##################################
# Definitions
##################################

def move_exit(ser):
    move_h = int(0./H_STEP)
    move_v = int(150./V_STEP)
    
    code = "M:W+P"+str(move_h)+"-P"+str(move_v)+"\r\n"
    ser.write(b"D:1S500F5000R50\r\n")
    ser.readline()
    ser.write(b"D:2S500F5000R50\r\n")
    ser.readline()
    ser.write(code.encode())
    ser.readline()
    ser.write(b"G:\r\n")
    ser.readline()
    
    ser.write(b"!:\r\n")
    a=str(ser.readline())
    while True:
        if a == BUSY:
            time.sleep(0.5)
            ser.write(b"!:\r\n")
            a=str(ser.readline())
            print(a)
        else:
            break
    ser.read_all()
    ser.write(b"Q:\r\n")
    print("Current position is", ser.readline())
    

def check_cmd(ser):
    a=str(ser.readline())
    print(a)
    if a != str(b'OK\r\n'):
        move_exit(ser)
        exit(1)


def check_move(ser):
    ser.write(b"!:\r\n")
    a=str(ser.readline())
    while True:
        if a == BUSY:
            time.sleep(0.5)
            ser.write(b"!:\r\n")
            a=str(ser.readline())
            print(a)
        else:
            break
    ser.read_all()
    ser.write(b"Q:\r\n")
    time.sleep(1)
    print("Current position is", ser.read_all())
##################################
# Main routine
##################################
if __name__ == '__main__':


    print("Emergency exit.")
    ser = serial.Serial(port="/dev/ttyUSB0",baudrate=9600, write_timeout=10, timeout=10)
    ser.write(b"L:E\r\n")
    check_cmd(ser)
    move_exit(ser)
    ser.close()
