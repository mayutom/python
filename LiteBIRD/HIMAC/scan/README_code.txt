ステージ駆動の準備
	Manual操作で初期位置を設定する。
	Reset を押し、初期位置を0,0に設定する。
	Host 操作に切り替える。


pattern.py	XY stageをHost制御するためのコマンドを絶対座標で生成する。スキャンの経路や、サンプル面上での相対ビーム照射量の図を出力できる。横方向に1往復したら縦に1step進み、また横方向に1往復する。縦横入れ替えることも可能。

  -h, --help            show this help message and exit
  -sh [S_H], --sample_horizontal [S_H]
                        horizontal sample size(mm)
  -sv [S_V], --sample_vertival [S_V]
                        vertical sample size(mm)
  -b [B], --beam [B]    beam length(mm)  ビームはvertical, horizontal両方向で同じ大きさであると仮定
  -s [STEP], --step [STEP]
                        step length(mm)
  -v [VEL], --velocity [VEL]
                        scan velocity(mm/sec)
  -t [OBS_TIME], --observation_time [OBS_TIME] 速度とサンプルサイズから、サンプルを一掃するのに必要な時間と、何周スキャンできるかを計算する
                        observation time(sec)
  -f [FILE], --file [FILE]
                        cmd output file name
  -o [OUTPUT], --image file [OUTPUT]
                        image output file name (without .pdf)

	スキャンパタンはshape_a, shape_b関数で定義している
	相対座標のコマンドを取得したい場合、265行目でabsolute/relativeのコメントアウトを変更する



scan.py		サンプルの中心にビームが当たるようにセットし、実行する。サンプルの角まで移動してから、コマンドファイルを読み取りスキャンを実行する。コマンドファイルの最終行まで完了したら、再度中心に戻る。

  -f,--file		cmd file name


move_to_zero.py	はじめにResetで設定した0,0座標へ戻る。

exit.py		scan.py実行中、緊急時に動作を停止して指定した距離だけ相対的に移動し、サンプルをブームから退避させる。

