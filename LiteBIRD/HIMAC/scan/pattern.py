# 2021/06/16 M. Tominaga

################################
# Imports
################################
import serial
import time
import argparse
import numpy as np
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

################################
# Parameters
################################

WAIT_SEC=0. #s
STAGE_RANGE = 200. #mm
V_MIN = -1000 #pulse
V_MAX = 99559 #pulse
H_MIN = -1000 #pulse
H_MAX = 99595 #pulse

RESOLUTION = 10. # Image resolution to the l_h/l_v

V_STEP = STAGE_RANGE/(V_MAX - V_MIN) #mm/pulse
H_STEP = STAGE_RANGE/(H_MAX - H_MIN) #mm/pulse

################################
# Class
################################
class ser:
	def __init__(self, ser, s_v, s_h, b, step, vel, time, margin):#step mm
		# 		self.s_v = s_v / V_STEP #mm -> pulse
		# 		self.s_h = s_h / H_STEP #mm -> pulse
		self.b_mm = b #mm
		self.time = time #s
		self.vel_mm = vel #mm/sec
		self.step = step #mm

		s_v += margin # mm
		s_h += margin # mm

		self.l_v = s_v // self.step +1#(s_v / self.step + 0.5)//2 * 2 + 1 # scan lines
		self.l_h = s_h // self.step +1#(s_h / self.step + 0.5)//2 * 2 + 1

		self.s_h = self.l_h * self.step / H_STEP #  scan range (pulse)
		self.s_v = self.l_v * self.step / V_STEP #  scan range (pulse)
		self.s_h_mm = self.l_h * self.step #  scan range (mm)
		self.s_v_mm = self.l_v * self.step #  scan range (mm)

		self.ser = ser
		self.pos_h=0.
		self.pos_v=0.
		self.pos_H=0.
		self.pos_V=0.

		print("Sample size (horizontal) %.02f (pulse), (vertical) %.02f (pulse)\n"%(self.s_h,self.s_v))
		print("Beam size %.02f (mm), observation time %.02f (sec)\n"%(self.b_mm,self.time))

	def calc_scan(self):
		print("Input observation time is %.02f sec."%self.time)

		self.scan_time = 0.
		self.shape = []


		if self.vel_mm == None:
			self.vel_mm = min(self.s_h_mm, self.s_v_mm)/(REL*WAIT_SEC) #mm/sec
		else:
			self.vel_mm = float(self.vel_mm)

		self.vel_h = self.vel_mm / H_STEP # pulse/sec
		self.vel_v = self.vel_mm / V_STEP # pulse/sec

		if self.vel_h < 1 or self.vel_h >20000 or self.vel_v < 1 or self.vel_v >20000:
			print("The velocity is out of range(1--20000). Velocity %.02f (mm/sec), Vel_h %.02f (pulse/sec), Vel_v %.02f (pulse/sec) "%(self.vel_mm, self.vel_h, self.vel_v))
			val = input("Change scan velocity to (mm/sec):")
			self.vel_mm = val

			self.vel_h = self.vel_mm / H_STEP # pulse/sec
			self.vel_v = self.vel_mm / V_STEP # pulse/sec


		print("Scan velocity is %.02f (mm/sec), Vel_h %.02f (pulse/sec), Vel_v %.02f (pulse/sec) "%(self.vel_mm, self.vel_h, self.vel_v))

		self.shape.append(shape_a(self.l_h, self.l_v, self.s_h, self.s_v))
		self.shape.append(shape_b(self.l_h, self.l_v, self.s_h, self.s_v))
	def calc_path(self):
		pos = np.array([0.,0.])#([float(self.pos_h), float(self.pos_v)])
		path = []
		path.append(np.array(pos))
		time_cnt = 0.
		timeline = []
		timeline.append(time_cnt)

		if int(len(self.shape))!=2:
			print("Warning.")

		dt_h = self.s_h/(self.l_h*RESOLUTION)/self.vel_h # sec
		dt_v = self.s_v/(self.l_v*RESOLUTION)/self.vel_v # sec

		for i in range(int(len(self.shape))):
			for j in range(int(len(self.shape[i]))):
				time_cnt += WAIT_SEC
				pos += np.array([0.,0.])

				path.append(np.array(pos))

				timeline.append(time_cnt)
				move_dis = 0

				if self.shape[i][j][0][1]==0:
					dt = dt_h
					vel = self.vel_h
				else:
					dt = dt_v
					vel = self.vel_v

				while True:
					if move_dis <= self.shape[i][j][1]:
						time_cnt += dt
						move_dis += vel*dt

						pos += np.array(self.shape[i][j][0]) * dt * vel
						timeline.append(time_cnt)

						path.append(np.array(pos))

					elif move_dis > self.shape[i][j][1] and move_dis < self.shape[i][j][1]:
						time_cnt += dt
						move_dis += vel*dt

						pos += np.array(self.shape[i][j][0]) * dt * vel
						timeline.append(time_cnt)

						path.append(np.array(pos))
					else:
						break



		# check scan number
		num_scan = self.time//timeline[-1]
		print("You can scan for %.02f times (%.01d sec/scan). "%(num_scan, timeline[-1]))
		val = input("Scan iterations :")
		num_scan = int(val)

		self.path = path

		self.timeline = timeline

		return num_scan

	def gen_img(self, output, fig="True"):
		Mu_x = self.b_mm / H_STEP / 2
		Mu_y = self.b_mm / V_STEP / 2
		Xs = np.linspace(0,self.s_h,int(self.l_h*RESOLUTION))
		Ys = np.linspace(0,self.s_v,int(self.l_v*RESOLUTION))
		#print(Xs, Ys)
		Path = np.array(self.path)
		Zs_canvas = np.array([[0.0 for X in Xs] for Y in Ys])

		Xc = (Xs[0] + Xs[-1] ) / 2.0
		Yc = (Ys[0] + Ys[-1] ) / 2.0

		Zs_template = calc_gau(Xs, Ys, Xc, Yc, Mu_x, Mu_y)

		Zss=[] # stores each shifted image.
		pos_x = []
		pos_y = []
		for i, [Xp, Yp] in enumerate(Path):
			if i>0:
				dX = int((Xp - Xc) / self.s_h * self.l_h * RESOLUTION)
				dY = int((Yp - Yc) / self.s_v * self.l_v * RESOLUTION)

				Zs = shift_image(Zs_template, dX, dY) * (self.timeline[i] - self.timeline[i-1])
				Zss.append(Zs)
				Zs_canvas = Zs_canvas + Zs
				pos_x.append(Xp/ self.s_h * self.l_h * RESOLUTION)
				pos_y.append(Yp/ self.s_v * self.l_v * RESOLUTION)
		self.Z = Zss

		if fig=="True":
			sns.heatmap(Zs_canvas)
			plt.savefig(output+"_img.pdf")
			plt.close()

			plt.plot(pos_x, pos_y)
			plt.savefig(output+"_track.pdf")
			plt.close()
			plt.plot(self.path)
			plt.savefig(output+"_path.pdf")
			plt.close()




	def calc_img(self):
		ave_z = np.mean(self.Z)
		max_z = np.max(self.Z)
		min_z = np.min(self.Z)
		sigma = max(max_z-ave_z, ave_z-min_z)/ave_z -1
		print("Scan smoothness is %.02f."%(sigma))

	def check_pos(self, f=None): # check current position
		if f==None:
			self.ser.write(b"Q:\r\n")
			pos = str(self.ser.readline())
			self.pos_h = int(pos.split(",")[0].split("'")[1].replace(" ",""))
			self.pos_v = int(pos.split(",")[1].replace(" ",""))
			print("Current position is", self.pos_h, self.pos_v)
		else:
			f.write("Q:\n")

	def check_center(self):
		self.center_h = self.pos_h
		self.center_v = self.pos_v
		print("Set center position", self.center_h, self.center_v)

	def move_to_center(self, f):
		f.write("A:W+P0+P0")

	def move_to_zero(self, f):
		move_h = self.center_h - self.s_h / 2
		move_v = self.center_v - self.s_v / 2
		if move_h >0:
			f.write("M:1+P"+str(int(move_h))+"\n")
		else:
			f.write("M:1-P"+str(int(-move_h))+"\n")

		if move_v >0:
			f.write("M:2+P"+str(int(move_v))+"\n")
		else:
			f.write("M:2-P"+str(int(-move_v))+"\n")
		self.pos_H, self.pos_V = move_h, move_v

	def check_height(self):
		# check scan height
		if self.pos_v + self.s_v > V_MAX:
			print("Warning: the current position is too high to scan all surface. Change the position.")

		else:
			print("The current position is okey to scan all surface. Continue.")

	def gen_vel(self,f):
		f.write("D:1S"+str(int(self.vel_h))+"F"+str(int(self.vel_h))+"R0"+"\n")
		f.write("D:2S"+str(int(self.vel_v))+"F"+str(int(self.vel_v))+"R0"+"\n")
		print("Set velocity (pulse/sec)", self.vel_h, self.vel_v)

	def gen_cmd(self, f):
		for i in range(int(len(self.shape))):
			for j in range(int(len(self.shape[i]))):
				#if self.shape[i][j][0][1]==0:
				#	vel = self.vel_h
				#else:

				#vel = self.vel_v
				self.pos_H += self.shape[i][j][0][0]*self.shape[i][j][1]
				self.pos_V += self.shape[i][j][0][1]*self.shape[i][j][1]

				# for relative
				#f.write(move(self.shape[i][j][0])+str(int(self.shape[i][j][1]))+"\n")

				# for absolute
				f.write(move2(self.pos_H,self.pos_V)+"\n")
	def clear(self):
		pass

##################################
# Definitions
##################################
def gaussian(x, y, xc, yc, mu_x, mu_y, d_thres=3.0):
    d = ( (x - xc) ** 2.0 + (y - yc) ** 2.0 ) / mu_x / mu_y
    if (d > d_thres):
        f = 0
    else:
        f = np.exp(-0.5 * d)
    return f

def calc_gau(Xs, Ys, Xc, Yc, Mu_x, Mu_y):
    Zs = np.array([[gaussian(X, Y, Xc, Yc, Mu_x, Mu_y) for X in Xs] for Y in Ys])
    return Zs


def shift_image(Zs, dx, dy):
    Zs = np.roll(Zs, dy, axis=0)
    Zs = np.roll(Zs, dx, axis=1)
    if dy>0:
        Zs[:dy, :] = 0
    elif dy<0:
        Zs[dy:, :] = 0
    if dx>0:
        Zs[:, :dx] = 0
    elif dx<0:
        Zs[:, dx:] = 0
    return Zs

def shape_a(l_x, l_y, s_x, s_y):
	#
	# *******
	# *******
	# *
	# *******
	# *******

	track = []
	for i in range(int(l_y)+1):
		track.append([(1,0),s_x])
		track.append([(-1,0),s_x])
		if i < int(l_y):
			track.append([(0,1),s_y//l_y])
	return track

def shape_b(l_x, l_y, s_x, s_y):
	#
	# *******
	# *******
	# *
	# *******
	# *******

	track = []
	for i in range(int(l_x)+1):
		track.append([(0,-1),s_y])
		track.append([(0,1),s_y])
		if i < int(l_x):
			track.append([(1,0),s_x//l_x])
	return track

def move(direction):
	if direction[0] == 1 and direction[1] == 0:
		return str("M:1+P")

	elif direction[0] == -1 and direction[1] ==0:
		return str("M:1-P")

	elif direction[0] == 0 and direction[1] == 1:
		return str("M:2+P")

	elif direction[0] == 0 and direction[1] == -1:
		return str("M:2-P")

def move2(pos_H, pos_V):
	if pos_H >= 0 and pos_V >= 0:
		return str("A:W+P%d+P%d"%(pos_H, pos_V))
	elif pos_H >= 0 and pos_V < 0:
		return str("A:W+P%d-P%d"%(pos_H, -pos_V))
	elif pos_H < 0 and pos_V >= 0:
		return str("A:W-P%d+P%d"%(-pos_H, pos_V))
	elif pos_H < 0 and pos_V < 0:
		return str("A:W-P%d-P%d"%(-pos_H, -pos_V))



##################################
# Main routine
##################################
if __name__ == '__main__':

	#
	# command-line parser
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'-sh','--sample_horizontal',
		help='horizontal sample length(mm)',
		dest='s_h',
		default = 50.,
		nargs='?'
		)
	parser.add_argument(
		'-sv','--sample_vertival',
		help='vertical sample length(mm)',
		dest='s_v',
		default = 50.,
		nargs='?'
		)
	parser.add_argument(
		'-b','--beam',
		help='beam length(mm)',
		dest='b',
		default = 10.,
		nargs='?'
		)
	parser.add_argument(
		'-s','--step',
		help='step length(mm)',
		dest='step',
		default = 10.,
		nargs='?'
		)
	parser.add_argument(
		'-v','--velocity',
		help='scan velocity(mm/sec)',
		dest='vel',
		default = None,
		nargs='?'
		)
	parser.add_argument(
		'-t','--observation_time',
		help='observation time(sec)',
		dest='obs_time',
		default = 1800,
		nargs='?'
		)
	parser.add_argument(
		'-f','--file',
		help='cmd output file name',
		dest='file',
		default = "cmd.txt",
		nargs='?'
		)
	parser.add_argument(
		'-o','--image file',
		help='image output file name (without .pdf)',
		dest='output',
		default = "test",
		nargs='?'
		)

	args = parser.parse_args()
	s_h = float(args.s_h)
	s_v = float(args.s_v)
	step = float(args.step)
	b = float(args.b)
	vel = float(args.vel)
	obs_time = float(args.obs_time)
	file_name = args.file
	output = args.output

	#
	# Main process
	#

	#(1) Setting
	print("-------------------")
	print("(1) Set position.")
	print("-------------------")
	my_serial = None #serial.Serial(port="/dev/ttyUSB0",baudrate=9600,write_timeout=10, timeout=10)
	my_class = ser(my_serial, s_v, s_h, b, step, vel, obs_time, margin=5)
	#my_class.move_to_zero()

	# 	val = "n"
	# 	while True:
	# 		if val == "n":
	# 			my_class.check_pos()
	# 			my_class.check_height()
	# 			val = input("Is the position okey? -> y/n:")
	# 		else:
	# 			break


	#(2) Scan test
	print("-------------------")
	print("(2) Calculate scans. Check the image.")
	print("-------------------")
	my_class.check_center()
	val = "n"
	while True:
		if val == "n":
			my_class.calc_scan()

			num_scan = my_class.calc_path()
			my_class.gen_img(fig="True", output=output)
			my_class.calc_img()
			val = input("Is scan strategy okey? -> y/n:")

			if val == "n":
				print("Current step is %.02f (mm)"%step)
				step = float(input("Change step to (mm):"))
				my_class = ser(my_serial, s_v, s_h, b, step, vel, obs_time)
				#my_class.check_pos()
				#my_class.check_height()

		else:
			break


	#(3) Generate cmd.
	print("-------------------")
	print("(3) Generate commands.")
	print("-------------------")
	f = open(file_name, 'w')
	#my_class.check_pos()
	#my_class.check_center()
	my_class.move_to_zero(f)


	my_class.check_pos(f)
	my_class.gen_vel(f)
	for i in range(int(num_scan)):
	# move to original pos
		my_class.gen_cmd(f)


	f.close()


