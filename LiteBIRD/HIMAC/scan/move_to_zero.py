import numpy as np
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
from sys import exit
import serial

BUSY = str(b'B\r\n')

def check_move(ser):
    ser.readline()
    ser.write(b"!:\r\n")
    a=str(ser.readline())
    while True:
        if a == BUSY:
            time.sleep(0.5)
            ser.write(b"!:\r\n")
            a=str(ser.readline())
            print(a)
        else:
            break
    ser.read_all()
    ser.write(b"Q:\r\n")
    print("Current position is", ser.readline())

ser = serial.Serial(port="/dev/ttyUSB0",baudrate=9600,write_timeout=10, timeout=10)

ser.write(b"A:W+P0+P0\r\n")
ser.readline()
ser.write(b"G:\r\n")
check_move(ser)


ser.close()
